# upload-service

POST 
upload file to server

    /api/v1/upload HTTP/1.1   
    Host: localhost:9090
    X-Upload-File: testFile.txt
    Content-Length: 1024
    
    Http status response:
    "201 CREATED"
    or
    "409 CONFLICT"
    
GET 
get "progress bar" of active uploads

    /api/v1/upload/progress HTTP/1.1
    Host: localhost:9090
    
    Response Example:
    {
        "uploads": [
            {
                "id": "testFile.txtd0ac5930-5b4c-417b-8d5b-f2592abd1d852020-02-19T18:04:37.898973",
                "size": 52,
                "uploaded": 50
            },
            {
                "id": "testFile.txt4a831176-294c-4dc0-83e0-fdc0876872822020-02-19T18:04:38.501337",
                "size": 52,
                "uploaded": 25
            },
            {
                "id": "testFile.txt594691c3-68cf-43b8-b37d-ce4619ab48152020-02-19T18:04:38.558851",
                "size": 52,
                "uploaded": 24
            },
            {
                "id": "testFile.txte06cdbdc-cb7e-49ef-8e0b-54817019841b2020-02-19T18:04:38.765067",
                "size": 52,
                "uploaded": 16
            },
            {
                "id": "testFile.txte8a4011d-165b-4aca-8420-325960f108ae2020-02-19T18:04:38.811333",
                "size": 52,
                "uploaded": 14
            },
            {
                "id": "testFile.txt2c89e394-4c84-4b09-963d-8370eb87a34a2020-02-19T18:04:39.098982",
                "size": 52,
                "uploaded": 4
            },
            {
                "id": "testFile.txtfb1a6c60-3053-42c1-b40f-cb934275c74a2020-02-19T18:04:39.102635",
                "size": 52,
                "uploaded": 3
            }
        ]
    }
    
GET 
get list of all uploaded files

    /api/v1/upload/duration
    Host: localhost:9090
    
    Response Example:
    {
        "uploadDuration": [
                "fileName": "testFile.txt6daf8f36-52a3-470a-a793-81d5838ca0fb-2020-02-19T18:04:37.675103",
                "timeToUpload": "1422 milliseconds"
            },
            {
                "fileName": "testFile.txt2849bdde-02b1-4317-8f7b-66daf12724a5-2020-02-19T18:04:37.838571",
                "timeToUpload": "1365 milliseconds"
            },
            {
                "fileName": "testFile.txtd0ac5930-5b4c-417b-8d5b-f2592abd1d85-2020-02-19T18:04:37.898973",
                "timeToUpload": "1368 milliseconds"
            },
            {
                "fileName": "testFile.txt4a831176-294c-4dc0-83e0-fdc087687282-2020-02-19T18:04:38.501337",
                "timeToUpload": "1462 milliseconds"
            },
            {
                "fileName": "testFile.txt594691c3-68cf-43b8-b37d-ce4619ab4815-2020-02-19T18:04:38.558851",
                "timeToUpload": "1459 milliseconds"
            },
            {
                "fileName": "testFile.txte06cdbdc-cb7e-49ef-8e0b-54817019841b-2020-02-19T18:04:38.765067",
                "timeToUpload": "1451 milliseconds"
            },
            {
                "fileName": "testFile.txte8a4011d-165b-4aca-8420-325960f108ae-2020-02-19T18:04:38.811333",
                "timeToUpload": "1460 milliseconds"
            },
            {
                "fileName": "testFile.txt2c89e394-4c84-4b09-963d-8370eb87a34a-2020-02-19T18:04:39.098982",
                "timeToUpload": "1434 milliseconds"
            },
            {
                "fileName": "testFile.txtfb1a6c60-3053-42c1-b40f-cb934275c74a-2020-02-19T18:04:39.102635",
                "timeToUpload": "1452 milliseconds"
            }
        ]
    }
    