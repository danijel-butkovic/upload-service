package com.dbutkovic.uploadservice.controllers;

import com.dbutkovic.uploadservice.controllers.response.UploadedFilesListResponse;
import com.dbutkovic.uploadservice.controllers.response.UploadingFilesListResponse;
import com.dbutkovic.uploadservice.controllers.response.utils.ResponseMapper;
import com.dbutkovic.uploadservice.services.UploadFileService;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.bind.annotation.RestController;

import javax.servlet.http.HttpServletRequest;
import java.io.IOException;

@RestController
public class UploadFileController {

    private final UploadFileService uploadFileService;

    private UploadFileController(UploadFileService uploadFileService) {
        this.uploadFileService = uploadFileService;
    }

    @GetMapping(value = "/api/v1/upload/duration")
    public ResponseEntity<UploadedFilesListResponse> getUploadingFiles() {
        return ResponseEntity.ok(ResponseMapper.toUploadedFilesListResponse(uploadFileService.getProcessedFiles()));
    }

    @GetMapping(value = "/api/v1/upload/progress")
    public ResponseEntity<UploadingFilesListResponse> getUploadStatus() {
        return ResponseEntity.ok(ResponseMapper.toUploadingFilesListResponse(uploadFileService.getProcessingFiles()));
    }

    @PostMapping(path = "/api/v1/upload")
    public ResponseEntity<Void> fileUpload(HttpServletRequest httpServletRequest,
                                           @RequestHeader(name = "X-Upload-File") String fileName) throws IOException {

        uploadFileService.uploadFile(httpServletRequest.getInputStream(),
                httpServletRequest.getContentLengthLong(),
                fileName);

        return ResponseEntity.status(HttpStatus.CREATED).build();
    }
}
