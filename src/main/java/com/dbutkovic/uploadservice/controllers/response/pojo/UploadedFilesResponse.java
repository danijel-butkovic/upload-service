package com.dbutkovic.uploadservice.controllers.response.pojo;

import com.fasterxml.jackson.annotation.JsonInclude;

import javax.validation.constraints.Size;
import java.util.Objects;

@JsonInclude(JsonInclude.Include.NON_NULL)
public class UploadedFilesResponse {

    @Size
    private String fileName;

    private String timeToUpload;

    public UploadedFilesResponse() {
        // for jackson
    }

    public UploadedFilesResponse(Builder builder){
        this.fileName = builder.fileName;
        this.timeToUpload = builder.timeToUpload;
    }

    public static Builder newBuilder() {
        return new Builder();
    }

    public String getFileName() {
        return fileName;
    }

    public void setFileName(String fileName) {
        this.fileName = fileName;
    }

    public String getTimeToUpload() {
        return timeToUpload;
    }

    public void setTimeToUpload(String timeToUpload) {
        this.timeToUpload = timeToUpload;
    }

    @Override
    public String toString() {
        return "UploadedFiles{" +
                "fileName='" + fileName + '\'' +
                ", timeToUpload='" + timeToUpload + '\'' +
                '}';
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof UploadedFilesResponse)) return false;
        UploadedFilesResponse that = (UploadedFilesResponse) o;
        return Objects.equals(fileName, that.fileName);
    }

    public static class Builder {
        private String fileName;
        private String timeToUpload;

        public Builder withFileName(String val) {
            fileName = val;
            return this;
        }

        public Builder withTimeToUpload(String val) {
            timeToUpload = val;
            return this;
        }

        public UploadedFilesResponse build() {
            return new UploadedFilesResponse(this);
        }
    }

}