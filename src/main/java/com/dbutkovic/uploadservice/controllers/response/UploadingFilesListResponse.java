package com.dbutkovic.uploadservice.controllers.response;

import com.dbutkovic.uploadservice.controllers.response.pojo.UploadingFilesResponse;
import com.fasterxml.jackson.annotation.JsonInclude;

import java.util.List;

@JsonInclude(JsonInclude.Include.NON_NULL)
public class UploadingFilesListResponse {

    private List<UploadingFilesResponse> uploads;

    public UploadingFilesListResponse() {
        // for jackson
    }

    public UploadingFilesListResponse(List<UploadingFilesResponse> uploads) {
        this.uploads = uploads;
    }

    public List<UploadingFilesResponse> getUploads() {
        return uploads;
    }
}
