package com.dbutkovic.uploadservice.controllers.response.utils;

import com.dbutkovic.uploadservice.controllers.response.UploadedFilesListResponse;
import com.dbutkovic.uploadservice.controllers.response.UploadingFilesListResponse;
import com.dbutkovic.uploadservice.controllers.response.pojo.UploadedFilesResponse;
import com.dbutkovic.uploadservice.controllers.response.pojo.UploadingFilesResponse;
import com.dbutkovic.uploadservice.services.pojo.UploadedFiles;
import com.dbutkovic.uploadservice.services.pojo.UploadingFiles;
import com.dbutkovic.uploadservice.utils.FileUtils;

import java.util.List;
import java.util.stream.Collectors;

public class ResponseMapper {

    private ResponseMapper() {
//        utils
    }

    public static UploadedFilesListResponse toUploadedFilesListResponse(List<UploadedFiles> uploadedFilesList) {
        return new UploadedFilesListResponse(
                uploadedFilesList.stream().map(
                        uploadedFiles ->
                                UploadedFilesResponse
                                        .newBuilder()
                                        .withFileName(uploadedFiles.getFileName())
                                        .withTimeToUpload(uploadedFiles.getTimeToUpload() + " milliseconds")
                                        .build()).collect(Collectors.toList()));
    }

    public static UploadingFilesListResponse toUploadingFilesListResponse(List<UploadingFiles> uploadingFilesList) {
        return new UploadingFilesListResponse(
                uploadingFilesList.stream().map(
                        uploadingFiles ->
                                UploadingFilesResponse
                                        .newBuilder()
                                        .withId(uploadingFiles.getId())
                                        .withSize(FileUtils.bytesToMegabytes(uploadingFiles.getSize()))
                                        .withUploaded(FileUtils.bytesToMegabytes(uploadingFiles.getUploaded()))
                                        .build()).collect(Collectors.toList()));
    }
}
