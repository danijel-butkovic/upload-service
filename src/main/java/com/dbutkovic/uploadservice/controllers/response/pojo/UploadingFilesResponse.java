package com.dbutkovic.uploadservice.controllers.response.pojo;

import com.fasterxml.jackson.annotation.JsonInclude;

@JsonInclude(JsonInclude.Include.NON_NULL)
public class UploadingFilesResponse {

    private String id;

    private long size;

    private long uploaded;

    public UploadingFilesResponse() {
        // for jackson
    }

    public UploadingFilesResponse(Builder builder) {
        this.id = builder.id;
        this.size = builder.size;
        this.uploaded = builder.uploaded;
    }

    public static Builder newBuilder() {
        return new Builder();
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public long getSize() {
        return size;
    }

    public void setSize(long size) {
        this.size = size;
    }

    public long getUploaded() {
        return uploaded;
    }

    public void setUploaded(long uploaded) {
        this.uploaded = uploaded;
    }

    @Override
    public String toString() {
        return "UploadingFiles{" +
                "id='" + id + '\'' +
                ", size=" + size +
                ", uploaded=" + uploaded +
                '}';
    }

    public static class Builder {
        private String id;
        private long size;
        private long uploaded;

        public Builder withId(String val) {
            id = val;
            return this;
        }

        public Builder withSize(long val) {
            size = val;
            return this;
        }

        public Builder withUploaded(long val) {
            uploaded = val;
            return this;
        }

        public UploadingFilesResponse build() {
            return new UploadingFilesResponse(this);
        }

    }
}
