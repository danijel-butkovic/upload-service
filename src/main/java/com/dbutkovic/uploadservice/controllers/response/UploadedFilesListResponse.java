package com.dbutkovic.uploadservice.controllers.response;

import com.dbutkovic.uploadservice.controllers.response.pojo.UploadedFilesResponse;
import com.fasterxml.jackson.annotation.JsonInclude;

import java.util.List;

@JsonInclude(JsonInclude.Include.NON_NULL)
public class UploadedFilesListResponse {

    private List<UploadedFilesResponse> uploadDuration;

    public UploadedFilesListResponse() {
        // for jackson
    }

    public UploadedFilesListResponse(List<UploadedFilesResponse> uploadDuration) {
        this.uploadDuration = uploadDuration;
    }

    public List<UploadedFilesResponse> getUploadDuration() {
        return uploadDuration;
    }
}
