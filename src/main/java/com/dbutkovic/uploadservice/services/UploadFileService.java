package com.dbutkovic.uploadservice.services;

import com.dbutkovic.uploadservice.ex.DuplicateFileException;
import com.dbutkovic.uploadservice.ex.FileUploadingException;
import com.dbutkovic.uploadservice.services.pojo.UploadedFiles;
import com.dbutkovic.uploadservice.services.pojo.UploadingFiles;
import com.dbutkovic.uploadservice.utils.FileUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.time.LocalDateTime;
import java.util.LinkedList;
import java.util.List;
import java.util.concurrent.locks.ReentrantReadWriteLock;

@Service
public class UploadFileService {

    private static final Logger logger = LoggerFactory.getLogger(UploadFileService.class);
    private static final int BUFFER_SIZE = 8192;
    private static final ReentrantReadWriteLock lock = new ReentrantReadWriteLock();

    private List<UploadedFiles> processedFiles = new LinkedList<>() {
    };

    private List<UploadingFiles> processingFiles = new LinkedList<>() {
    };

    public void uploadFile(InputStream inputStream, long contentLength, String fileName) {


        lock.writeLock().lock();
        if (processingFiles.stream().anyMatch(p -> p.getOriginalName().equals(fileName))) {
            logger.error("File is already uploading... ");
            lock.writeLock().unlock();
            throw new DuplicateFileException(fileName);
        }

        logger.info("File {} does not exist. Starting upload", fileName);

        var startTime = System.currentTimeMillis();
        var currentTimestamp = LocalDateTime.now();

        var uploadFile = UploadingFiles
                .newBuilder()
                .withOriginalName(fileName)
                .withId(fileName + currentTimestamp)
                .withSize(contentLength)
                .withUploaded(0)
                .build();

        processingFiles.add(uploadFile);

        lock.writeLock().unlock();

        try {
            upload(inputStream, contentLength, fileName, uploadFile);
        } catch (Exception e) {
            logger.error("Writing into file went wrong. Unable to save file...");
            throw new FileUploadingException("Writing into file went wrong. Unable to save file. " + e.getMessage());
        }

        var totalTime = String.valueOf(System.currentTimeMillis() - startTime);

        logger.info("File " + fileName + " has uploaded successfully. " +
                "It took " + totalTime + " milliseconds.");

        var uploadedFile = UploadedFiles
                .newBuilder()
                .withOriginalName(fileName)
                .withFileName(fileName + "-" + currentTimestamp)
                .withTimeToUpload(totalTime)
                .build();
        processedFiles.remove(uploadedFile);
        processedFiles.add(uploadedFile);
        processingFiles.remove(uploadFile);

    }

    private void upload(InputStream inputStream, long contentLength,
                        String fileName, UploadingFiles uploadFile) throws IOException {

        var convertFile = new File("/tmp/" + fileName);
        var buffer = new byte[BUFFER_SIZE];
        var totalUploaded = 0L;
        var fileOut = new FileOutputStream(convertFile);
        var lastPrintOn = 0L;

        var content = 0;
        while ((content = inputStream.read(buffer)) != -1) {
            totalUploaded += content;
            fileOut.write(buffer, 0, content);
            uploadFile.setUploaded(totalUploaded);

            if (totalUploaded >= (lastPrintOn + FileUtils.megabytesToBytes(5))) {
                logger.info("File uploaded size is: " + totalUploaded +
                        ". File original size is " + contentLength);
                lastPrintOn = totalUploaded;
            }
        }
        inputStream.close();
        fileOut.close();
        logger.info("File uploaded.");
    }

    public List<UploadedFiles> getProcessedFiles() {
        return processedFiles;
    }

    public List<UploadingFiles> getProcessingFiles() {
        return processingFiles;
    }
}
