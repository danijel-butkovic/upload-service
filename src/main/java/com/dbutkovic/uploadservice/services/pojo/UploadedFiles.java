package com.dbutkovic.uploadservice.services.pojo;

import com.fasterxml.jackson.annotation.JsonInclude;

import java.util.Objects;

@JsonInclude(JsonInclude.Include.NON_NULL)
public class UploadedFiles {

    private String originalName;

    private String fileName;

    private String timeToUpload;

    public UploadedFiles() {
        // for jackson
    }

    public UploadedFiles(Builder builder){
        this.originalName = builder.originalName;
        this.fileName = builder.fileName;
        this.timeToUpload = builder.timeToUpload;
    }

    public static Builder newBuilder() {
        return new Builder();
    }

    public String getOriginalName() {
        return originalName;
    }

    public void setOriginalName(String originalName) {
        this.originalName = originalName;
    }

    public String getFileName() {
        return fileName;
    }

    public void setFileName(String fileName) {
        this.fileName = fileName;
    }

    public String getTimeToUpload() {
        return timeToUpload;
    }

    public void setTimeToUpload(String timeToUpload) {
        this.timeToUpload = timeToUpload;
    }

    @Override
    public String toString() {
        return "UploadedFiles{" +
                "originalName='" + originalName + '\'' +
                ", fileName='" + fileName + '\'' +
                ", timeToUpload='" + timeToUpload + '\'' +
                '}';
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof UploadedFiles)) return false;
        UploadedFiles that = (UploadedFiles) o;
        return Objects.equals(originalName, that.originalName);
    }

    public static class Builder {
        private String originalName;
        private String fileName;
        private String timeToUpload;

        public Builder withOriginalName(String val) {
            originalName = val;
            return this;
        }

        public Builder withFileName(String val) {
            fileName = val;
            return this;
        }

        public Builder withTimeToUpload(String val) {
            timeToUpload = val;
            return this;
        }

        public UploadedFiles build() {
            return new UploadedFiles(this);
        }
    }

}