package com.dbutkovic.uploadservice.services.pojo;

import com.fasterxml.jackson.annotation.JsonInclude;

@JsonInclude(JsonInclude.Include.NON_NULL)
public class UploadingFiles {

    private String originalName;

    private String id;

    private long size;

    private long uploaded;

    public UploadingFiles() {
        // for jackson
    }

    public UploadingFiles(Builder builder) {
        this.originalName = builder.originalName;
        this.id = builder.id;
        this.size = builder.size;
        this.uploaded = builder.uploaded;
    }

    public static Builder newBuilder() {
        return new Builder();
    }

    public String getId() {
        return id;
    }

    public String getOriginalName() {
        return originalName;
    }

    public void setOriginalName(String originalName) {
        this.originalName = originalName;
    }

    public void setId(String id) {
        this.id = id;
    }

    public long getSize() {
        return size;
    }

    public void setSize(long size) {
        this.size = size;
    }

    public long getUploaded() {
        return uploaded;
    }

    public void setUploaded(long uploaded) {
        this.uploaded = uploaded;
    }

    @Override
    public String toString() {
        return "UploadingFiles{" +
                "originalName='" + originalName + '\'' +
                ", id='" + id + '\'' +
                ", size=" + size +
                ", uploaded=" + uploaded +
                '}';
    }

    public static class Builder {
        private String originalName;
        private String id;
        private long size;
        private long uploaded;

        public Builder withOriginalName(String val) {
            originalName = val;
            return this;
        }

        public Builder withId(String val) {
            id = val;
            return this;
        }

        public Builder withSize(long val) {
            size = val;
            return this;
        }

        public Builder withUploaded(long val) {
            uploaded = val;
            return this;
        }

        public UploadingFiles build() {
            return new UploadingFiles(this);
        }

    }
}
