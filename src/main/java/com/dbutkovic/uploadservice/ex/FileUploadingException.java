package com.dbutkovic.uploadservice.ex;

public class FileUploadingException extends RuntimeException {

    public FileUploadingException(String message) {
        super(message);
    }

}
