package com.dbutkovic.uploadservice.ex;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.RestControllerAdvice;

@RestControllerAdvice
public class ControllerExceptionHandler {

    private static Logger logger = LoggerFactory.getLogger(ControllerExceptionHandler.class);

    @ExceptionHandler(FileUploadingException.class)
    public ResponseEntity<String> handleValidationException(FileUploadingException e) {
        logger.warn("Input stream failed", e);
        return ResponseEntity.badRequest().body(e.getMessage());
    }

    @ExceptionHandler(DuplicateFileException.class)
    public ResponseEntity<String> handleDuplicateFileException(DuplicateFileException e) {
        logger.warn("File already exists", e);
        return ResponseEntity.status(HttpStatus.CONFLICT).body(e.getMessage());
    }
}
