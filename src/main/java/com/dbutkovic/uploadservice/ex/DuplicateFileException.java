package com.dbutkovic.uploadservice.ex;

public class DuplicateFileException extends RuntimeException {

    public DuplicateFileException(String message) {
        super(message);
    }
}
