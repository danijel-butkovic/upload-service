package com.dbutkovic.uploadservice.utils;

public class FileUtils {

    private FileUtils() {
//        utils
    }

    public static long bytesToMegabytes(long bytes){
        return bytes / (1000 * 1000);
    }

    public static long megabytesToBytes(long megabytes){
        return megabytes * 1000 * 1000;
    }
}
