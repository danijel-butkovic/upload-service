package com.dbutkovic.uploadservice;


import org.junit.Before;
import org.junit.Test;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.HttpStatus;
import org.springframework.test.context.web.WebAppConfiguration;
import org.springframework.web.client.RestTemplate;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.Collections;
import java.util.LinkedList;
import java.util.List;
import java.util.UUID;
import java.util.concurrent.ArrayBlockingQueue;
import java.util.concurrent.BlockingQueue;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.ThreadPoolExecutor;
import java.util.concurrent.TimeUnit;
import java.util.stream.Collectors;

import static org.assertj.core.api.Assertions.assertThat;

@WebAppConfiguration
public class ControllerTest {

    private static Logger logger = LoggerFactory.getLogger(ControllerTest.class);

    // Before test do:
    // Linux: Create file /tmp/test.txt.
    // Comment after first run of test

    @Before
    public void makeFile() throws IOException {
        var convertFile = new File("/tmp/test.txt");
        var fileOut = new FileOutputStream(convertFile);

        for (int i = 0; i < 300000; i++) {
            for (int j = 0; j < 5; j++) {
                fileOut.write("Test123Test123Test123Test123Test123".getBytes());
            }
            fileOut.write("\n".getBytes());
        }
    }

    @Test
    public void given100request_whenSend100Request_ThenExpectOk() throws InterruptedException {
        ExecutorService executor = new CustomThreadPoolExecutor(
                10, 100, 10000, TimeUnit.SECONDS, new ArrayBlockingQueue<Runnable>(100));

        List<UploadJob> collectObjects = new LinkedList<>();

        for (int i = 0; i < 100; i++) {
            try {
                var uploadFile = new UploadJob();
                collectObjects.add(uploadFile);
                executor.submit(uploadFile);
            } catch (Exception e) {
                System.err.println("Caught exception: " + e.getMessage());
            }
        }
        executor.shutdown();
        executor.awaitTermination(10, TimeUnit.MINUTES);

        var results = collectObjects.stream().map( status -> status.status).collect(Collectors.toList());

        logger.info("true: " + Collections.frequency(results, true));
        logger.info("false: " + Collections.frequency(results, false));

        assertThat(collectObjects.size()).isEqualTo(100);
        assertThat(Collections.frequency(results, true)).isEqualTo(100);
    }


    @Test
    public void whenSendTwoFileWithSameNameParallel_thenExpectConflict() throws InterruptedException {
        var executor = Executors.newFixedThreadPool(10);

        List<UploadJobWithSameName> objects = new LinkedList<>();

        for (int i = 0; i < 10; i++) {
            try {
                var uploadFile = new UploadJobWithSameName();
                objects.add(uploadFile);
                executor.submit(uploadFile);
            } catch (Exception e) {
                System.err.println("Caught exception: " + e.getMessage());
            }
        }
        executor.shutdown();
        executor.awaitTermination(1, TimeUnit.MINUTES);

        var results = objects.stream().map( status -> status.status).collect(Collectors.toList());

        logger.info(String.valueOf(Collections.frequency(results, true)));

        assertThat(Collections.frequency(results, true)).isEqualTo(1);
    }

    public static class UploadJob implements Runnable {

        boolean status;

        @Override
        public void run() {

            RestTemplate restTemplate = new RestTemplate();
            UUID uuid = UUID.randomUUID();

            byte[] body = new byte[0];
            try {
                body = Files.readAllBytes(Paths.get("/tmp/test.txt"));
            } catch (IOException e) {
                e.printStackTrace();
            }

            HttpHeaders httpHeaders = new HttpHeaders();
            httpHeaders.add("X-Upload-File", "testFile.txt" + uuid);
            HttpEntity<Object> requestEntity = new HttpEntity<>(body, httpHeaders);

            var response = restTemplate.exchange("http://localhost:9090/api/v1/upload",
                    HttpMethod.POST,
                    requestEntity,
                    Void.class);

            status = response.getStatusCodeValue() == HttpStatus.CREATED.value();
        }
    }

    public static class UploadJobWithSameName implements Runnable {

        boolean status;

        @Override
        public void run() {

            RestTemplate restTemplate = new RestTemplate();

            byte[] body = new byte[0];
            try {
                body = Files.readAllBytes(Paths.get("/tmp/test.txt"));
            } catch (IOException e) {
                e.printStackTrace();
            }

            HttpHeaders httpHeaders = new HttpHeaders();
            httpHeaders.add("X-Upload-File", "testFile.txt");
            HttpEntity<Object> requestEntity = new HttpEntity<>(body, httpHeaders);

            var response = restTemplate.exchange("http://localhost:9090/api/v1/upload",
                    HttpMethod.POST,
                    requestEntity,
                    Void.class);

            status = response.getStatusCodeValue() == HttpStatus.CREATED.value();
        }
    }

    static class CustomThreadPoolExecutor extends ThreadPoolExecutor {

        public CustomThreadPoolExecutor(int corePoolSize, int maximumPoolSize,
                                        long keepAliveTime, TimeUnit unit, BlockingQueue<Runnable> workQueue) {
            super(corePoolSize, maximumPoolSize, keepAliveTime, unit, workQueue);
        }

        @Override
        public void afterExecute(Runnable r, Throwable t) {
            super.afterExecute(r, t);
            if (t != null) {
                throw new RuntimeException("Error processing multi thread upload");
            }
        }
    }
}
